module Razoom
  class Categories
    include Razoom::Resource

    def self.all(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/categories#{params}", token)
    end

    def self.find(token, id)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/categories/#{id}", token)
    end

    def self.create(token, data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/categories", token, data)
    end

    def self.update(token, id, data)
      Razoom::Methods.request('put', "#{Razoom.base_uri}/categories/#{id}", token, data)
    end
  end
end
