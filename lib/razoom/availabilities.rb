module Razoom
  class Availabilities
    include Razoom::Resource

    def self.find(token, activity_id, date_and_time)
      Razoom::Methods.request('get',
        "#{Razoom.base_uri}/products/#{activity_id}/availabilities/#{date_and_time}", token)
    end

    def self.all(token, activity_id)
      Razoom::Methods.request('get',
        "#{Razoom.base_uri}/products/#{activity_id}/availabilities#{params}", token)
    end
  end
end