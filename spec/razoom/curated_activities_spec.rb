require 'spec_helper'

describe Razoom::CuratedActivities do
  it 'create a curated activity' do
    VCR.use_cassette('curated_activity_create') do
      expect(Razoom::CuratedActivities.create(token, activity_id, curated_activity).class).to eq(Hash)
    end
  end

  it 'updates a curated activity' do
    VCR.use_cassette('curated_activity_update') do
      expect(Razoom::CuratedActivities.update(token, activity_id, curated_activity).class).to eq(Hash)
    end
  end
end