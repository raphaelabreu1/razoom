require 'spec_helper'

describe Razoom do
  it 'has a version number' do
    expect(Razoom::VERSION).not_to be nil
  end

  it 'has app_id' do
    expect(Razoom).to respond_to(:app_id)
  end

  it 'has secret' do
    expect(Razoom).to respond_to(:secret)
  end

  it 'has the user' do
    VCR.use_cassette('razoom_me') do
      user = Razoom.me(token)
      expect(user).to have_key(:name)
      expect(user).to have_key(:email)
    end
  end
end
