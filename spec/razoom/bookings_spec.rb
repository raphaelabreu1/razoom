require 'spec_helper'

describe Razoom::Bookings do
  it 'create a booking' do
    VCR.use_cassette('booking_create') do
      expect(Razoom::Bookings.create(token, booking).class).to eq(Hash)
    end
  end

  it 'request a existing bookings' do
    VCR.use_cassette('booking_find') do
      expect(Razoom::Bookings.find(token, booking_id)).to have_key(:booking)
    end
  end
end
