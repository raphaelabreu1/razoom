module Razoom
  class Sales
    include Razoom::Resource

    def self.find(token, id)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/sales/#{id}", token)
    end

    def self.new(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/sales/new", token)
    end

    def self.create(token, data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/sales", token, data)
    end

    def self.destroy(token, id)
      Razoom::Methods.request('delete', "#{Razoom.base_uri}/sales/#{id}", token)
    end
  end
end
