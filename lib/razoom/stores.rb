module Razoom
  class Stores
    include Razoom::Resource

    def self.find(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/stores", token)
    end

    def self.create(token, data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/stores", token, data)
    end

    def self.update(token, data)
      Razoom::Methods.request('put', "#{Razoom.base_uri}/stores", token, data)
    end
  end
end