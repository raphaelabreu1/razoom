require 'spec_helper'

describe Razoom::Availabilities do
  it 'should return an array of availabilitilies' do
    VCR.use_cassette('availabilities_all') do
      expect(Razoom::Availabilities.where(token, activity_id, { month: 1, year: 16 }).class).to eq(Array)
    end

  end

  it 'should have more than 1 items on array of availabilities' do
    VCR.use_cassette('availabilities_all') do
      expect(Razoom::Availabilities.where(token, activity_id, { month: 1, year: 16 }).count).to be > 1
    end
  end
end
