module Razoom
  class Affiliates
    include Razoom::Resource

    def self.create(token, data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/affiliates", token, data)
    end

    def self.find(token, id)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/affiliates/#{id}", token)
    end
  end
end
