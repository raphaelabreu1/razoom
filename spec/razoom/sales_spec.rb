require 'spec_helper'

describe Razoom::Sales do
  it 'request an specified sale' do
    VCR.use_cassette('sales_find') do
      expect(Razoom::Sales.find(token, sale_id)).to have_key(:date)
    end
  end

  it 'create a sale' do
    VCR.use_cassette('sale_create') do
      expect(Razoom::Sales.create(token, sale).class).to eq(Hash)
    end
  end
end