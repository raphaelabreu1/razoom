module Razoom
  class Activities
    include Razoom::Resource

    def self.all(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/activities#{params}", token)
    end

    def self.find(token, id)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/activities/#{id}", token)
    end

    def self.create(token, data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/activities", token, data)
    end

    def self.update(token, id, data)
      Razoom::Methods.request('put', "#{Razoom.base_uri}/activities/#{id}", token, data)
    end

    def self.preview(token, search)
      Razoom::Products.preview(token, search)
    end
  end
end
