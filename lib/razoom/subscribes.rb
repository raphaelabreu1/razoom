module Razoom
  class Subscribes
    def self.create(data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/subscribes", nil, data)
    end
  end
end
