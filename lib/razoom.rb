require 'razoom/resource'
require 'razoom/activities'
require 'razoom/affiliates'
require 'razoom/availabilities'
require 'razoom/departures'
require 'razoom/bookings'
require 'razoom/cities'
require 'razoom/channels'
require 'razoom/countries'
require 'razoom/curated_activities'
require 'razoom/methods'
require 'razoom/providers'
require 'razoom/sales'
require 'razoom/stores'
require 'razoom/channel_products'
require 'razoom/prices'
require 'razoom/products'
require 'razoom/users'
require 'razoom/subscribes'
require 'razoom/categories'
require 'razoom/promo_code'
require 'razoom/version'
require 'oauth2'

module Razoom
  API_VERSION = 'v1'

  def self.base_uri
    "#{api_endpoint}/#{API_VERSION}"
  end

  def self.client
    @client
  end

  # @param config: { endpoint: '', api_id: '', secret: '' }
  def self.initialize(config = {})
    @config = config
    @client ||= OAuth2::Client.new(@config[:app_id], @config[:secret], site: base_uri)
  end

  def self.get_token_by_password(name, password)
    client.password.get_token(name, password)
  end

  def self.me(token)
    Razoom::Methods.request('get', "#{base_uri}/users/me", token)
  end

  def self.search(token, q = '*')
    Razoom::Methods.request('get', "#{base_uri}/search?q=#{q}", token)
  end

  def self.api_endpoint
    @config[:endpoint]
  end
end
