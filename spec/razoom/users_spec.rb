require 'spec_helper'

describe Razoom::Users do
  it 'request the user store' do
    VCR.use_cassette('user_store') do
      store = Razoom::Users.store(token)
      expect(store).to have_key(:business_name)
    end
  end
end