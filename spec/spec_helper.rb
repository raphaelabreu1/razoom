$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'vcr'
require 'razoom'
require 'razoom/resource'
require 'razoom/activities'
require 'razoom/availabilities'
require 'razoom/cities'
require 'razoom/channels'
require 'razoom/curated_activities'
require 'razoom/methods'
require 'razoom/providers'
require 'razoom/sales'
require 'razoom/stores'
require 'razoom/users'

VCR.configure do |config|
  config.cassette_library_dir = "spec/fixtures/vcr_cassettes"
  config.hook_into :webmock # or :fakeweb
end

# TODO: Colocar o vcr nas requisições a API
# TODO: Criar um mock de cada um dos itens abaixo
Razoom.initialize(endpoint: 'http://localhost:4000',
                  api_id: '73515b9230b2aa843692cfe8396e0c15da31770af33cf9bbface1de3bd51047f',
                  secret: 'dd2f1c01af7153456f5550e852da27a19c9a3222fb946f6c2f37de58c70ecbdc')


def token
  # TODO: Não pode ficar assim. A requisição está sendo feita a cada teste #
  VCR.use_cassette('activities_all') do
    @token = Razoom.get_token_by_password('razoom', 'razoom123').token
  end
end

def activity_id
  '556666d55241625b75000000'
end

def booking_id
  '559dad715241623ca32a0000'
end

def provider_id
  '5543f442524162166b000000'
end

def product_id
  '55bd225d52416213bf000010'
end

def booking
  {
      booking: {
          activity_id: activity_id,
          date: '2016-05-05',
          start_time: '20:00',
          quantity: '2',
          customer: {
              name: 'Customer Name',
              email: 'customer@example',
              phone: '99-99999-9999',
              language: 'Português',
              chosen_pickup: 'Chosen Pickup Test',
              passport_id: '12345321',
              observations: 'Observations test'
          },
          travellers: [{
                           name: 'Traveller Test',
                           passport_id: '12345678',
                           email: 'traveller@example.com',
                           phone: '21-0000-0000',
                       }]

      }
  }
end

def provider
  {
      provider: {
          name: 'Test Provider Gem',
          corporate_name: 'Test Provider Gem S.A.',
          cnpj: '000.000.000/0000/11',
          email: 'provider@example.com',
          website: 'http://www.razoom.com.br',
          location: {
              location: 'Lugar Qualquer',
              district: 'Bairro Qualquer',
              city: 'Cidade Qualquer',
              state: 'RJ',
              country: 'Brasil',
              cep: '25.950-000'
          },
          responsible: {
              name: 'Test Responsible Name',
              email: 'responsible@example.com',
              cpf: '123.321.12-12'
          },
          bank_account: {
              bank_code: '1234',
              bank_name: 'Qualquer Banco S.A.',
              account_type: 'corrente',
              bank_branch: '1123-32',
              account_number: '9999-0',
              recipient: 'Recipient Test Name',
              recipient_cpf_cnpj: '123123123123'
          },
          contacts: [{
                         phone: '0000-0000',
                         name: 'Responsible Name Test',
                         email: 'provider@example.com'
                     }]
      }
  }
end

def activity
  {
      :provider_id => provider_id,
      :activity => {
          :duration => 90,
          :availability => {
              :type => 'weekly',
              :weekly => [{
                              :start_time => '08:00',
                              :weekday => 1
                          }]
          }
      }
  }
end

def curated_activity
  {
      curated_activity: {
          type: 'tour',
          privacy_type: 'Regular',
          name: 'Curated Activity Test',
          languages: [],
          description: '',
          min_pax: 2,
          max_pax: 10,
          available_location: {
              type: 'pickup',
              location: 'Rua Etc'
          },
          location: {
              location: 'Local qualquer',
              district: 'Bairro Qualquer',
              city: 'Cidade Qualquer',
              state: 'RJ',
              country: 'Brasil'
          },
          requirement: {
              hours_in_advance: 48,
              cancellation: {
                  hours_in_advance: 72
              }
          },
          price: {
              type: 'single',
              single: {
                  value: 100.00
              }
          },
          infos: [
              { title: 'info_included', description: 'Incluído tudo' },
              { title: 'info_not_included', description: 'Não está não incluído' },
              { title: 'info_recommended', description: 'Tudo é recmendado' },
              { title: 'info_minimum_age', description: 'Não tem' },
              { title: 'info_other_requirements', description: 'Info adicional' }
          ]
      }
  }
end

def sale_id
  '55afcd075241623702000000'
end

def sale
  {
      sale: {
          bookings: [booking_id]
      }
  }
end

def channel_id
  '55a5c8495241624122000000'
end

def channel
  {
      channel: {
          name: 'Razoom Test',
          description: 'Razoom Test'
      }
  }
end

def store
  {
      channel_id: channel_id.to_s,
      store: {
          business_name: 'Store One Test',
          legal_name: 'Store One Test S.A.',
          phone_number: '(21) 9999-9999',
          responsible_email: 'store@razoom.com.br',
          cpf_cnpj: '123456789',
          address: {
              street: 'Av. da Rua Etc, número ',
              number: '56',
              district: 'Botafogo',
              city: 'Rio de Janeiro',
              state: 'RJ',
              country: 'Brasil'
          }
      }
  }
end

def store_id
  '55ae8ded5241623eec000000'
end
