module Razoom
  class ChannelProducts
    include Razoom::Resource

    def self.all(token, provider_id = nil)
      url = "#{Razoom.base_uri}/channel_products"
      url += "?provider_id=#{provider_id}" unless provider_id.nil?
      Razoom::Methods.request('get', url, token)
    end

    def self.update(token, id, data)
      Razoom::Methods.request('put', "#{Razoom.base_uri}/channel_products/#{id}", token, data)
    end
  end
end
