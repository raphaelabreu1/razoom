module Razoom
  class Products
    include Razoom::Resource

    def self.all(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/products#{params}", token)
    end

    def self.find(token, id, args={})
      set_params(args)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/products/#{id}#{params}", token)
    end

    def self.preview(token, data)
      search_data = ''

      if data.is_a?(String)
        search_data = "search=#{data}"
      else
        search_data = "search=#{data[:search]}&city=#{data[:city]}"
      end

      Razoom::Methods.request('get', "#{Razoom.base_uri}/products/preview?#{search_data}", token)
    end

    def self.related(token, id, args={})
      set_params(args)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/products/#{id}/related#{params}", token)
    end
  end
end
