require 'spec_helper'

describe Razoom::Stores do
  it 'should return an object with items' do
    VCR.use_cassette('stores_all_items') do
      expect(Razoom::Stores.all(token)).to have_key(:items)
    end
  end

  it 'should return an array of stores' do
    VCR.use_cassette('stores_all_array') do
      expect(Razoom::Stores.all(token)[:items].class).to eq(Array)
    end
  end

  it 'should have more than 1 items on array of stores' do
    VCR.use_cassette('stores_all_length') do
      expect(Razoom::Stores.all(token)[:items].count).to be >= 1
    end
  end

  it 'request an specified store' do
    VCR.use_cassette('store_find') do
      expect(Razoom::Stores.find(token, store_id)).to have_key(:business_name)
    end
  end

  it 'create an store' do
    VCR.use_cassette('store_create') do
      expect(Razoom::Stores.create(token, store).class).to eq(Hash)
    end
  end

  it 'updates an store' do
    VCR.use_cassette('store_update') do
      expect(Razoom::Stores.update(token, store_id, store).class).to eq(Hash)
    end
  end
end