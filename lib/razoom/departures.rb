module Razoom
  class Departures
    include Razoom::Resource

    def self.find(token, id)
      Razoom::Methods.request('get',
        "#{Razoom.base_uri}/departures/#{id}", token)
    end
  end
end
