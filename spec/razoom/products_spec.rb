require 'spec_helper'

describe Razoom::Products do
  it 'should return an object with items' do
    VCR.use_cassette('products_all_items') do
      expect(Razoom::Products.all(token)).to have_key(:items)
    end
  end

  it 'should return an array of products' do
    VCR.use_cassette('products_all_array') do
      expect(Razoom::Products.all(token)[:items].class).to eq(Array)
    end
  end

  it 'should have more than 1 items on array of products' do
    VCR.use_cassette('products_all_length') do
      expect(Razoom::Products.all(token)[:items].count).to be >= 1
    end
  end

  it 'request an specified product' do
    VCR.use_cassette('products_find') do
      expect(Razoom::Products.find(token, product_id)).to have_key(:name)
    end
  end

  it 'request all products by cities' do
    VCR.use_cassette('products_all_cities') do
      products_in_rio = Razoom::Products.where(token, nil, { cities: ['Rio de Janeiro'], page: 1})
      expect(products_in_rio).to have_key(:items)
      expect(products_in_rio[:paging][:total]).to eq("331")
    end
  end

  it 'request all cities by two cities' do
    VCR.use_cassette('products_all_two_cities') do
      products_in_two_cities = Razoom::Products.where(token, nil, { cities: ['Rio de Janeiro', 'Mangaratiba'], page: 1})
      expect(products_in_two_cities).to have_key(:items)
      expect(products_in_two_cities[:paging][:total]).to eq("332")
    end
  end
end
