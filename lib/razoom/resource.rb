module Razoom
  module Resource
    def self.included(klass)
      @query_string = nil

      def klass.where(token, id = nil, args={})
        set_params(args)
        if id.nil?
          all(token)
        else
          all(token, id)
        end
      end

      def klass.params
        @query_string
      end

      def klass.set_params(args={})
        @query_string = hash_to_params(args)
      end

      private
      def klass.hash_to_params(hash)
        params = ''
        if hash.keys.any?
          params = '?'
          hash.keys.each do |key|
            if hash[key].is_a? Array
              params = params.concat(to_array_param(hash, key))
            else
              params = params.concat("#{key}=#{encode(hash[key])}&")
            end
          end
        end
        params
      end

      def klass.to_array_param(hash, key)
        param = ''
        items = hash[key]
        items.each { |item| param << "#{key}[]=#{encode(item)}&" }
        param
      end

      def klass.encode(value)
        URI.encode(value.to_s)
      end
    end
  end
end
