module Razoom
  class Users
    include Razoom::Resource

    def self.all(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/users#{params}", token)
    end

    def self.find(token, id)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/users/#{id}", token)
    end

    def self.create(token, data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/users", token, data)
    end

    def self.update(token, id, data)
      Razoom::Methods.request('put', "#{Razoom.base_uri}/users/#{id}", token, data)
    end

    def self.enable(token, id)
      Razoom::Methods.request('put', "#{Razoom.base_uri}/users/#{id}/enable", token)
    end

    def self.disable(token, id)
      Razoom::Methods.request('put', "#{Razoom.base_uri}/users/#{id}/disable", token)
    end

    def self.store(token)
      Razoom::Stores.find(token)
    end

    def self.update_store(token, data)
      Razoom::Stores.update(token, data)
    end
  end
end
