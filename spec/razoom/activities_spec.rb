require 'spec_helper'

describe Razoom::Activities do
  it 'should return an object with items' do
    VCR.use_cassette('activities_all_items') do
      expect(Razoom::Activities.all(token)).to have_key(:items)
    end
  end

  it 'should return an array of activities' do
    VCR.use_cassette('activities_all_array') do
      expect(Razoom::Activities.all(token)[:items].class).to eq(Array)
    end
  end

  it 'should have more than 1 items on array of activities' do
    VCR.use_cassette('activities_all_length') do
      expect(Razoom::Activities.all(token)[:items].count).to be >= 1
    end
  end

  it 'request an specified activity' do
    VCR.use_cassette('activities_find') do
      expect(Razoom::Activities.find(token, activity_id)).to have_key(:name)
    end
  end

  it 'request all activities by cities' do
    VCR.use_cassette('activities_all_cities') do
      activities_in_rio = Razoom::Activities.where(token, nil, { cities: ['Rio de Janeiro'], page: 1})
      expect(activities_in_rio).to have_key(:items)
      expect(activities_in_rio[:paging][:total]).to eq("331")
    end
  end

  it 'request all cities by two cities' do
    VCR.use_cassette('activities_all_two_cities') do
      activities_in_two_cities = Razoom::Activities.where(token, nil, { cities: ['Rio de Janeiro', 'Mangaratiba'], page: 1})
      expect(activities_in_two_cities).to have_key(:items)
      expect(activities_in_two_cities[:paging][:total]).to eq("332")
    end
  end

  it 'create an activity' do
    VCR.use_cassette('activity_create') do
      expect(Razoom::Activities.create(token, activity).class).to eq(Hash)
    end
  end

  it 'updates an activity' do
    VCR.use_cassette('activity_update') do
      expect(Razoom::Activities.update(token, activity_id, activity).class).to eq(Hash)
    end
  end
end
