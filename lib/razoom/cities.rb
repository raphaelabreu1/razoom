module Razoom
  class Cities
    include Razoom::Resource

    def self.all(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/cities#{params}", token)
    end
  end
end
