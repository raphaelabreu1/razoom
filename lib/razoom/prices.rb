module Razoom
  class Prices
    include Razoom::Resource

    # @date_and_time
    # @quantity
    def self.all(token, activity_id)
      Razoom::Methods.request('get',
        "#{Razoom.base_uri}/products/#{activity_id}/prices#{params}", token)
    end
  end
end