module Razoom
  class Bookings
    include Razoom::Resource
    
    def self.all(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/bookings#{params}", token)
    end

    def self.create(token, data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/bookings", token, data)
    end

    def self.find(token, id)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/bookings/#{id}", token)
    end
  end
end
