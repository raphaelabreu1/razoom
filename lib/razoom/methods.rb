require 'rest_client'

module Razoom
  class Methods
    def self.request(method, url, token = nil, data = {})
      return {} if token.nil?

      access_token = OAuth2::AccessToken.new(Razoom.client, token)
      response = execute_request(method, url, access_token, data)
      if response.is_a?(Hash)
        response
      else
        JSON.parse(response, symbolize_names: true)
      end
    end

    private
    
    def self.execute_request(method, url, token = nil, data = {})
      case method.upcase
        when 'GET'
          token.get(url).body
        when 'POST'
          # TODO: Entender o motivo do Oauth2 não funcionar
          # Usei o RestClient para POST pq estou encontrando um problema tanto no
          # RestClient quanto no Oauth2 (Faraday) para fazer um POST de um objeto
          # com um children array, como os travellers de bookings
          # Estou usando a gem rest-client editada por mim #raphaelabreu1
          # https://github.com/raphaelabreu1/rest-client/commit/0df8f6e90594984560ce923bfd7ae266adb26044
          client = RestClient::Resource.new(url)
          client.post(data, { content_type: 'application/json', authorization: "Bearer #{token.token}" })
        when 'PUT'
          client = RestClient::Resource.new(url)
          client.put(data, { content_type: 'application/json', authorization: "Bearer #{token.token}" })
        when 'DELETE'
          token.delete(url).body
        else
          throw('Não implementado ainda')
      end
    rescue RestClient::Exception => e
      return e.http_body
    rescue => e
      return { errors: e.message }
    end
  end
end
