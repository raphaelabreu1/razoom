require 'spec_helper'

describe Razoom::Cities do
  it 'request all cities' do
    VCR.use_cassette('cities_all') do
      expect(Razoom::Cities.all(token).class).to eq(Array)
    end
  end

  it 'request all available cities' do
    VCR.use_cassette('cities_all') do
      expect(Razoom::Cities.all(token).count).to be >= 1
    end
  end
end
