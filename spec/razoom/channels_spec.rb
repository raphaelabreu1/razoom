require 'spec_helper'

describe Razoom::Channels do
  it 'should return an object with items' do
    VCR.use_cassette('channels_all_items') do
      expect(Razoom::Channels.all(token)).to have_key(:items)
    end
  end

  it 'should return an array of channels' do
    VCR.use_cassette('channels_all_array') do
      expect(Razoom::Channels.all(token)[:items].class).to eq(Array)
    end
  end

  it 'should have more than 1 items on array of channels' do
    VCR.use_cassette('channels_all_length') do
      expect(Razoom::Channels.all(token)[:items].count).to be >= 1
    end
  end

  it 'request an specified channel' do
    VCR.use_cassette('channel_find') do
      expect(Razoom::Channels.find(token, channel_id)).to have_key(:name)
    end
  end

  it 'create an channel' do
    VCR.use_cassette('channel_create') do
      expect(Razoom::Channels.create(token, channel).class).to eq(Hash)
    end
  end

  it 'updates an channel' do
    VCR.use_cassette('channel_update') do
      expect(Razoom::Channels.update(token, channel_id, channel).class).to eq(Hash)
    end
  end
end