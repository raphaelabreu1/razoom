require 'spec_helper'

describe Razoom::Providers do
  it 'should return an object with items' do
    VCR.use_cassette('providers_all_items') do
      expect(Razoom::Providers.all(token)).to have_key(:items)
    end
  end

  it 'should return an array of providers' do
    VCR.use_cassette('providers_all_array') do
      expect(Razoom::Providers.all(token)[:items].class).to eq(Array)
    end
  end

  it 'should have more than 1 items on array of providers' do
    VCR.use_cassette('providers_all_length') do
      expect(Razoom::Providers.all(token)[:items].count).to be >= 1
    end
  end

  it 'request an specified provider' do
    VCR.use_cassette('providers_find') do
      expect(Razoom::Providers.find(token, provider_id)).to have_key(:name)
    end
  end

  it 'create a provider' do
    VCR.use_cassette('provider_create') do
      expect(Razoom::Providers.create(token, provider).class).to eq(Hash)
    end
  end

  it 'updates a provider' do
    VCR.use_cassette('provider_update') do
      expect(Razoom::Providers.update(token, provider_id, provider).class).to eq(Hash)
    end
  end
end