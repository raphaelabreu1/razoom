module Razoom
  class Countries
    include Razoom::Resource

    def self.all(token)
      Razoom::Methods.request('get', "#{Razoom.base_uri}/countries", token)
    end
  end
end