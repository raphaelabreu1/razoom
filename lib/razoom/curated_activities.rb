module Razoom
  class CuratedActivities
    include Razoom::Resource

    def self.create(token, activity_id, data)
      Razoom::Methods.request('post', "#{Razoom.base_uri}/activities/#{activity_id}/curated", token, data)
    end

    def self.update(token, id, data)
      Razoom::Methods.request('put', "#{Razoom.base_uri}/activities/curated/#{id}", token, data)
    end
  end
end